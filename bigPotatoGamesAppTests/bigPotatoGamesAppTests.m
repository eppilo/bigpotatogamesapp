//
//  bigPotatoGamesAppTests.m
//  bigPotatoGamesAppTests
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GamesDatabase.h"

@interface bigPotatoGamesAppTests : XCTestCase

@end

@implementation bigPotatoGamesAppTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFetchAllGames {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    GamesDatabase *gDB = [[GamesDatabase alloc] init];
    XCTAssertNotNil([gDB fetchAllGames]);
    NSLog(@"GAMES = \n%@",[gDB fetchAllGames]);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
