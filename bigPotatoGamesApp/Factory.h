//
//  Factory.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 24/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UITableView;
@class UITableViewCell;
@class GameData;
@class MainViewCellPrototype;

@interface Factory : NSObject

+ (MainViewCellPrototype *)tableView:(UITableView *)tableView
                          cell:(MainViewCellPrototype *)cell
                  withGameData:(GameData *)gd;

@end
