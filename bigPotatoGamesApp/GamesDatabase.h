//
//  GamesDatabase.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GameData;

@interface GamesDatabase : NSObject

- (NSDictionary *)fetchAllGames;
- (GameData *)fetchGameWithName: (NSString *)name;

@end
