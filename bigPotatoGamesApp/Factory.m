//
//  Factory.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 24/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "Factory.h"
#import <UIKit/UIKit.h>
#import "GameData.h"
#import "DescriptionView.h"
#import "UIColor+BPGExtensions.h"
#import "RainbowView.h"
#import "MainViewCellPrototype.h"

@implementation Factory

+ (MainViewCellPrototype *)tableView:(UITableView *)tableView
                          cell:(MainViewCellPrototype *)cell
                  withGameData:(GameData *)gd
{
    // Cell selection style
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Description view
    DescriptionView *dv = [[[NSBundle mainBundle] loadNibNamed: @"DescriptionView" owner: self options: nil]objectAtIndex: 0];
    dv.translatesAutoresizingMaskIntoConstraints = NO;
    dv.icon.image = [UIImage imageNamed:gd.iconName];
    dv.label.text = gd.title;
    [cell.contentView addSubview:dv];
    dv.backgroundColor = [UIColor colorWithHexString:gd.backgroundColor];
    NSDictionary *nameMap = @{@"dv" : dv};
    NSArray *const1 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[dv]-0-|"
                                                              options:0
                                                              metrics:nil
                                                                views:nameMap];
    NSArray *const2 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[dv]-0-|"
                                                              options:0
                                                              metrics:nil
                                                                views:nameMap];
    [cell.contentView addConstraints:const1];
    [cell.contentView addConstraints:const2];
    
    // Only give background view to the first cell
    if (![gd.title  isEqual: @"Super Game"]) {
        return cell;
    };
    
    
    // Backgroind View
    CGRect frame = CGRectMake(0, 0, 0, 0);
    RainbowView *bgv = [[RainbowView alloc] initWithFrame:frame];
    bgv.translatesAutoresizingMaskIntoConstraints = NO;
    [dv addBackgroundView:bgv];
    ///*
    NSLayoutConstraint *constX = [NSLayoutConstraint constraintWithItem:bgv
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:bgv.superview
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1
                                                               constant:0];
    [dv addConstraint:constX];
    NSLayoutConstraint *constY = [NSLayoutConstraint constraintWithItem:bgv
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:bgv.superview
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:0];
    
    [dv addConstraint:constY];
    
    NSLayoutConstraint *heightConstraint =
    [NSLayoutConstraint constraintWithItem:bgv
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:1.0
                                  constant:470.0];
    [bgv addConstraint:heightConstraint];
    
    NSLayoutConstraint *widthConstraint = [
     NSLayoutConstraint constraintWithItem:bgv
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:1.0
                                  constant:470.0];
    [bgv addConstraint:widthConstraint];
    
    CABasicAnimation *animation = [CABasicAnimation animation];
    animation.keyPath = @"transform.rotation.z";
    animation.toValue = @(M_PI_2 * 4);
    animation.duration = 10;
    animation.removedOnCompletion = YES;
    animation.repeatCount = HUGE_VALF;
    
    // Add hadle to animation and background layer
    cell.animation = animation;
    cell.backgroundLayer = bgv.layer;
    
    return cell;
}

@end
