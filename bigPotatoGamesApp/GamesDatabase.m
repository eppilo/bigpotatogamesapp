//
//  GamesDatabase.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "GamesDatabase.h"
#import "GameData.h"
#import <UIKit/UIKit.h>

@interface GamesDatabase()

@property NSDictionary *games;

@end

@implementation GamesDatabase

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"games" ofType:@"plist"];
        self.games = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    return self;
}

- (NSDictionary *)fetchAllGames
{
    // Deep copy. This simulates a brand new fetch from a database
    return [[NSDictionary alloc] initWithDictionary: self.games copyItems:YES];
}

- (GameData *)fetchGameWithName:(NSString *)name
{
    // Deep copy.
    NSDictionary *data = [[NSDictionary alloc] initWithDictionary:[self.games objectForKey:name]
                                                        copyItems:YES];
    //NSScanner *scanner = [NSScanner scannerWithString:[data objectForKey:@"backgroundColor"]];
    //unsigned color = 0;
    //[scanner scanHexInt:&color];
    GameData *gd = [[GameData alloc] initWithIconName:[data objectForKey:@"iconName"]
                                                title:[data objectForKey:@"title"]
                                     shortDescription:[data objectForKey:@"shortDescription"]
                                    extraCardIconName:[data objectForKey:@"extraCardIconName"]
                                       extraCardsData:[data objectForKey:@"extraCardData"]
                                instructionsFileNames:[data objectForKey:@"instructionsFileNames"]
                                           appletName:[data objectForKey:@"appletName"]
                                      backgroundColor:[data objectForKey:@"backgroundColor"]];
    
    return gd;
}

@end
