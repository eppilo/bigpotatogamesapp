//
//  MainViewControllerTableViewController.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "MainViewController.h"
#import "GamesDatabase.h"
#import "GamesIndex.h"
#import "DescriptionView.h"
#import "GameData.h"
#import "UIColor+BPGExtensions.h"
#import <CoreGraphics/CoreGraphics.h>
#import "RainbowView.h"
#import "Factory.h"
#import "MainViewCellPrototype.h"

@interface MainViewController ()

@property (nonatomic) GamesDatabase *games;
@property (nonatomic) GamesIndex *gamesIndex;
@property (nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize properties
    _games = [[GamesDatabase alloc] init];
    _gamesIndex = [[GamesIndex alloc] init];
    
    // Remove hairline
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // Background
    self.tableView.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    // NOT IMPLEMENTED
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_gamesIndex count];
}

- (MainViewCellPrototype *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MainViewCellPrototype *cell = (MainViewCellPrototype *)[tableView dequeueReusableCellWithIdentifier:@"MainViewCellPrototype" forIndexPath:indexPath];
    
    // Configure the cell...
    GameData *gd = [_games fetchGameWithName:[_gamesIndex gameNameAtIndex:indexPath.row]];
    cell = [Factory tableView:tableView cell:cell withGameData:gd];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath == _selectedIndexPath) {
        _selectedIndexPath = nil;
        MainViewCellPrototype *cell = (MainViewCellPrototype *)[tableView cellForRowAtIndexPath:indexPath];
        //if (cell.isAnimating){
            //[cell removeAnimationEventually];
        [cell pauseAnimation];
        //}
    } else {
        MainViewCellPrototype *previousCell = [tableView cellForRowAtIndexPath:_selectedIndexPath];
        [previousCell pauseAnimation];
        _selectedIndexPath = indexPath;
        MainViewCellPrototype *cell = (MainViewCellPrototype *)[tableView cellForRowAtIndexPath:indexPath];
        //if (!cell.isAnimating){
          //  [cell addAnimation];
        //}
        [cell startAnimation];
    }
    [tableView beginUpdates];
    [tableView endUpdates];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
    if (indexPath == _selectedIndexPath) {
        return 280;
    } else {
        return 140;
    }
}




/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
