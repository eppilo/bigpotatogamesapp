//
//  ExtraCardsData.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIColor;

@interface ExtraCardData : NSObject

@property (nonatomic) NSString *question;
@property (nonatomic) NSString *answer;

@end
