//
//  UIColor+BPGExtensions.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 23/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BPGExtensions)

+ (UIColor *)colorWithHexString: (NSString *)hex;

@end
