//
//  GamesIndex.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 19/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "GamesIndex.h"
#import "GameData.h"
#import "GamesDatabase.h"

@implementation GamesIndex
{
    NSArray *games;
}

// Designated initializer
- (instancetype)init
{
    self = [super init];
    if (self) {
        games = [NSArray arrayWithObjects:
                 @"SuperGame",
                 @"MegaGame",
                 @"ExcellentGame",
                 nil];
    }
    return self;
}

- (NSString *)gameNameAtIndex:(NSInteger)index
{
    if (index < [games count]) {
        return [games objectAtIndex:index];
    } else {
        return nil;
    }
}

- (NSUInteger)count
{
    return [games count];
}

@end
