//
//  CALayer+BPGExtensions.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 26/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "CALayer+BPGExtensions.h"

@implementation CALayer (BPGExtensions)

-(void)pauseLayer
{
    CFTimeInterval pausedTime = [self convertTime:CACurrentMediaTime() fromLayer:nil];
    self.speed = 0.0;
    self.timeOffset = pausedTime;
}

-(void)resumeLayer
{
    CFTimeInterval pausedTime = [self timeOffset];
    self.speed = 1.0;
    self.timeOffset = 0.0;
    self.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.beginTime = timeSincePause;
}

@end
