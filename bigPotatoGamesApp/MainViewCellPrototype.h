//
//  MainVIewCellPrototype.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CABasicAnimation;
@class CALayer;

@interface MainViewCellPrototype : UITableViewCell

@property (nonatomic) CABasicAnimation *animation;
@property (nonatomic) CALayer *backgroundLayer;
@property (nonatomic) BOOL isAnimating;

- (void)startAnimation;
- (void)pauseAnimation;

@end
