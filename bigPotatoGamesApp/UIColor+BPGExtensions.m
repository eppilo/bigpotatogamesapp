//
//  UIColor+BPGExtensions.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 23/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "UIColor+BPGExtensions.h"
#import <CoreGraphics/CoreGraphics.h>

@implementation UIColor (BPGExtensions)

+ (UIColor *)colorWithHexString: (NSString *)hex
{
    NSCharacterSet *cSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *cString = [[hex stringByTrimmingCharactersInSet:cSet] uppercaseString];
    
    if ([cString hasPrefix:@"#"]) {
        cString = [cString substringFromIndex:1];
    }
    
    if ([cString hasPrefix:@"0X"]) {
        cString = [cString substringFromIndex:2];
    }
    
    if ([cString length] != 6 ) {
        return [UIColor grayColor];
    }
    
    NSString *rString = [cString substringToIndex:2];
    NSString *gString = [[cString substringFromIndex:2] substringToIndex:2];
    NSString *bString = [[cString substringFromIndex:4] substringToIndex:2];
    unsigned int r = 0;
    unsigned int g = 0;
    unsigned int b = 0;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    CGFloat rr = (CGFloat)r / 255.0;
    CGFloat gg = (CGFloat)g / 255.0;
    CGFloat bb = (CGFloat)b / 255.0;
    
    return [UIColor colorWithRed:rr green:gg blue:bb alpha:1];
}

@end
