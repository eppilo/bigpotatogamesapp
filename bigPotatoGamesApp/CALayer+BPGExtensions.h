//
//  CALayer+BPGExtensions.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 26/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (BPGExtensions)

-(void)pauseLayer;
-(void)resumeLayer;

@end
