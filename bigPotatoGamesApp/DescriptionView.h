//
//  DescriptionView.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 19/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *buttonContainer;


- (void)addBackgroundView: (UIView *)view;

@end
