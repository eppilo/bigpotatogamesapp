//
//  Game.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIColor;

@interface GameData <NSCopying>: NSObject

@property (nonatomic) NSString *iconName;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *shortDescription;
@property (nonatomic) NSString *extraCardIconName;
@property (nonatomic) NSArray *extraCardsData;
@property (nonatomic) NSArray *instructionsFileNames;
@property (nonatomic) NSString *appletName;
@property (nonatomic) NSString *backgroundColor;

- (instancetype)initWithIconName:(NSString *)iconName
title:(NSString *)title
shortDescription:(NSString *)shortDescription
extraCardIconName:(NSString *)extraCardIconName
extraCardsData:(NSArray *)extraCardsData
instructionsFileNames:(NSArray *)instructionsFileNames
appletName:(NSString *)appletName
backgroundColor:(NSString *)backgroundColor;


@end
