//
//  MainVIewCellPrototype.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "MainViewCellPrototype.h"
#import <UIKit/UIKit.h>
#import "CALayer+BPGExtensions.h"

@implementation MainViewCellPrototype
{
    NSTimer *cancelAnimationTimer;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)startAnimation
{
    if (cancelAnimationTimer) {
        [cancelAnimationTimer invalidate];
        cancelAnimationTimer = nil;
    }
    if (!_isAnimating) {
        [CATransaction begin]; {
            [CATransaction setCompletionBlock:^{
                _isAnimating = NO;
            }];
            //_isAnimating = YES;
            [_backgroundLayer addAnimation:_animation forKey:@"basic"];
        } [CATransaction commit];
        _isAnimating = YES;
    }
}
    
- (void)pauseAnimation
{
    if (!cancelAnimationTimer) {
        cancelAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:(10)
                                                                target:self
                                                              selector:@selector(_cancelAnimation)
                                                              userInfo:nil
                                                               repeats:NO];
    }
}

- (void)_cancelAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSNumber *angle = [[_backgroundLayer presentationLayer] valueForKey:@"transform.rotation.z"];
        _backgroundLayer.transform = CATransform3DMakeRotation([angle floatValue], 0, 0, 1);
        [_backgroundLayer removeAllAnimations];
    });
}


@end
