//
//  RainbowView.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 23/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "RainbowView.h"
#import <CoreImage/CoreImage.h>

@implementation RainbowView
{
    NSUInteger currentColorIndex;
}

// Designated initializer
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

// This initializer MUST NOT be used.  RainbowView is never initialized with a NIB.
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    NSException *exception = [NSException exceptionWithName:@"Wrong initializer"
                                                     reason:@"Cannot call initWithCoder for RainbowView"
                                                   userInfo:nil];
    @throw exception;
}

- (void)drawRect:(CGRect)rect {
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    CGPoint center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
    CGFloat radius = MAX(self.bounds.size.height, self.bounds.size.width);
    
    // 9 is the number of slices.  That always needs to be a multiple of the number of colors (3 in this case".  Can be generalized later.
    CGFloat delta = 2 * M_PI / 9;
    CGFloat startAngle = 0;
    CGFloat endAngle = delta;
    currentColorIndex = 0;
    
    for (int i = 0; i < 9; i++) {
        UIBezierPath *path = [[UIBezierPath alloc] init];
        [path moveToPoint:center];
        [path addArcWithCenter:center radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
        [path addLineToPoint:center];
        [path closePath];
        [[[self colorsSequence] objectAtIndex:currentColorIndex] setFill];
        [path fill];
        startAngle += delta;
        endAngle += delta;
        if (currentColorIndex == [[self colorsSequence] count] - 1) {
            currentColorIndex = 0;
        } else {
            currentColorIndex += 1;
        }
    }
    
}

- (NSArray *)colorsSequence
{
    UIColor *c1 = [UIColor colorWithRed:149.0 / 255.0 green:203.0 / 255.0 blue:241.0 / 255.0 alpha:1];
    UIColor *c2 = [UIColor colorWithRed:238.0 / 255.0 green:126.0 / 255.0 blue:192.0 / 255.0 alpha:1];
    UIColor *c3 = [UIColor colorWithRed:192.0 / 255.0 green:238.0 / 255.0 blue:126.0 / 255.0 alpha:1];
    return [NSArray arrayWithObjects:c1, c2, c3, nil];
}

@end
