//
//  Game.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "GameData.h"
#import <UIKit/UIKit.h>

@implementation GameData

// Designated initializer
- (instancetype)initWithIconName:(NSString *)iconName
                        title:(NSString *)title
                shortDescription:(NSString *)shortDescription
               extraCardIconName:(NSString *)extraCardIconName
                  extraCardsData:(NSArray *)extraCardsData
           instructionsFileNames:(NSArray *)instructionsFileNames
                      appletName:(NSString *)appletName
                 backgroundColor:(NSString *)backgroundColor;
{
    self = [super init];
    if (self) {
        self.iconName = iconName;
        self.title = title;
        self.shortDescription = shortDescription;
        self.extraCardIconName = extraCardIconName;
        self.extraCardsData = extraCardsData;
        self.instructionsFileNames = instructionsFileNames;
        self.appletName = appletName;
        self.backgroundColor = backgroundColor;
    }
    return self;
}

- (instancetype)init
{
    self = [self initWithIconName:@"defaultIcon"
                            title:@"Default Title"
                 shortDescription:@"Default short description"
                extraCardIconName:@"defaultIconCardName"
                   extraCardsData:nil instructionsFileNames:[NSArray arrayWithObjects:@"defaultInstructionFileName", nil]
                       appletName:nil
                  backgroundColor:@"0x000000"];
    return self;
}

@end
