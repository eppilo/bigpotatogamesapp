//
//  MiddleView.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 19/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "DescriptionView.h"

@implementation DescriptionView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    _buttonContainer.layer.borderWidth = 4;
    _buttonContainer.layer.borderColor = [[UIColor whiteColor] CGColor];
    _buttonContainer.layer.cornerRadius = 25;
    _buttonContainer.layer.masksToBounds = YES;
}


// Designated initializer
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}

- (void)addBackgroundView:(UIView *)view
{
    [self insertSubview:view atIndex:0];
}

@end
