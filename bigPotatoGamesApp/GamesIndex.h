//
//  GamesIndex.h
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 19/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GameData;

@interface GamesIndex : NSObject

- (NSString *)gameNameAtIndex: (NSInteger)index;
- (NSUInteger)count;

@end
