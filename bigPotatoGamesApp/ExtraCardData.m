//
//  ExtraCardsData.m
//  bigPotatoGamesApp
//
//  Created by Paolo Longato on 15/04/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

#import "ExtraCardData.h"

@implementation ExtraCardData

- (instancetype)initWithQuestion: (NSString *)question
                       andAnswer: (NSString *)answer
{
    self = [super init];
    if (self) {
        self.question = question;
        self.answer = answer;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.question = @"Default question.";
        self.answer = @"Default answer";
    }
    return self;
}

@end
